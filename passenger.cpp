/* passenger.cpp
 *
 * A passenger class for simulation of loading an airplane.
 *
 * Seth McNeill
 * 2015 March 30
 */

#include "passenger.h"

passenger::passenger(int rn, bool uSC, bool oC, int reqAT, int sT)
{
  rowNum = rn;
  underSeatCarry = uSC;
  overheadCarry = oC;
  reqAisleTime = reqAT;
  seatTime = sT;
}


void passenger::setRowNum(int n) // set rowNum
{
  rowNum= n;
}


void passenger::setUnderCarry(bool y)
{
  underSeatCarry = y;
}


void passenger::setOverCarry(bool y)
{
  overheadCarry = y;
}


void passenger::setReqAisleTime(int t)
{
  reqAisleTime = t;
}


void passenger::resetSeatTime(void)// sets seatTime to 0
{
  seatTime = 0;
}


// seatTime++, should be called each step of simulation
void passenger::incSeatTime(void)
{
  seatTime++;
}


int passenger::getRow(void)
{
  return(rowNum);
}


bool passenger::getUnderCarry(void)
{
  return(underSeatCarry);
}


bool passenger::getOverCarry(void)
{
  return(overheadCarry);
}


int passenger::getReqAisleTime (void)
{
  return(reqAisleTime);
}

int passenger::getSeatTime(void)
{
  return(seatTime);
}


void passenger::printPassenger(void)
{
  cout << "row " << rowNum << ": underSeatCarry=" << underSeatCarry << 
       ", overheadCarry=" << overheadCarry << ", reqAisleTime=" << 
       reqAisleTime << ", seatTime=" << seatTime;
}