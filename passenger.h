/* passenger.h
 *
 * A passenger class for simulation of loading an airplane.
 *
 * Seth McNeill
 * 2015 March 30
 */

#include <iostream>
using namespace std;


class passenger
{
public:
  // Default constructor with default values
  passenger(int rn = 0, bool uSC = false, bool oC = false, int reqAT = 2, int sT = 0);
  void setRowNum(int n); // set rowNum
  void setUnderCarry(bool y);
  void setOverCarry(bool y);
  void setReqAisleTime(int t);
  void resetSeatTime(void); // sets seatTime to 0
  // seatTime++, should be called each step of simulation
  void incSeatTime(void);
  int getRow(void);
  bool getUnderCarry(void);
  bool getOverCarry(void);
  int getReqAisleTime (void);
  int getSeatTime(void);
  void printPassenger(void); // prints out formatted info
private:
  int rowNum; // row in air craft numbered from entrance,
    // first available row is 1
  bool underSeatCarry; // has an underseat carryon
  bool overheadCarry; // has an overhead carryon
  int reqAisleTime; // time for passenger to sit once at correct row
  int seatTime; // Total time to be seated, incremented each time step
} ;